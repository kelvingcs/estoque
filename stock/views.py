from django.shortcuts import render, redirect
from django.views.generic import ListView, UpdateView, DeleteView
from stock.models import Product


class ProductListView(ListView):
    model = Product

class OutOfStockListView(ListView):
    model = Product

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantity=0)

class AddStockView(UpdateView):
    model = Product
    fields = ['quantity']

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        product.quantity += int(self.request.POST['quantity'])
        product.save()
        return redirect('product-list')

class RemoveStockView(DeleteView):
    model = Product
    fields = ['quantity']

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        product.quantity -= int(self.request.POST['quantity'])
        product.save()
        return redirect('product-list')