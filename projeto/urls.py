from django.contrib import admin
from django.urls import path

from stock import views as stock_views


urlpatterns = [
    path('', stock_views.ProductListView.as_view(), name='product-list'),
    path('fora-de-estoque/',stock_views.OutOfStockListView.as_view(),name='out-of-stock'),
    path('adicionar-estoque/<int:pk>/',stock_views.AddStockView.as_view(),name='add-stock'),
    path('admin/', admin.site.urls),
    path('remover-estoque/<int:pk>/',stock_views.RemoveStockView.as_view(),name='remove-stock'),

]
